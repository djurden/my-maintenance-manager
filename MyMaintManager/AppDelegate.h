//
//  AppDelegate.h
//  MyMaintManager
//
//  Created by Dan Jurden on 5/25/13.
//  Copyright (c) 2013 Brazos Valley Computer Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
