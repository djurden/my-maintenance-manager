//
//  VehicleViewController.m
//  MyMaintManager
//
//  Created by Dan Jurden on 5/27/13.
//  Copyright (c) 2013 Brazos Valley Computer Services. All rights reserved.
//

#import "VehicleViewController.h"

@interface VehicleViewController ()

@end

@implementation VehicleViewController 

NSMutableArray *vehicles;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    vehicles = [[NSMutableArray alloc] initWithCapacity:3];
    vehicles[0] = @"One";
    vehicles[1] = @"Two";
    vehicles[2] = @"Three";
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return vehicles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    //UIButton *disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    cell.textLabel.font = [UIFont systemFontOfSize:17.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:17.0];
    cell.textLabel.text = vehicles[indexPath.row];
    cell.detailTextLabel.text = @"3,000";
    //cell.accessoryView = disclosureButton;
    
    return cell;
}



@end
